var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var GoatSchema   = new Schema({
	name: String
});

module.exports = mongoose.model('Goat', GoatSchema);