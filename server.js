
var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var router = express.Router();

app.listen(port);
console.log('started @ ' + port);


var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/goats');
var Goat = require('./app/models/goat');

app.use('/api', router);
router.use(function(req, res, next) {
	console.log('Something is happening.');
	next();
});

router.get('/', function(req, res) {
	res.json({ message: 'hello world' });
});

router.route('/goats')

	.post(function(req, res) {

		var goat = new Goat();
		goat.name = req.body.name;

		goat.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'Goat created!' });
		});

	})

	.get(function(req, res) {
		Goat.find(function(err, goats) {
			if (err)
				res.send(err);

			res.json(goats);
		});
	});

router.route('/goats/:goat_id')

	.get(function(req, res) {
		Goat.findById(req.params.goat_id, function(err, goat) {
			if (err)
				res.send(err);
			res.json(goat);
		});
	})

	.put(function(req, res) {

		Goat.findById(req.params.goat_id, function(err, goat) {

			if (err)
				res.send(err);

			goat.name = req.body.name;

			goat.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Goat updated!' });
			});

		});
	})

	.delete(function(req, res) {
		Goat.remove({
			_id: req.params.goat_id
		}, function(err, goat) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});




